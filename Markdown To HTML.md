# markdown to HTML convertor

## wyam

- enkel cmd prompt
- vergelijkbaar met markdown-it

    [Wyam download](https://atom.io/)

## markdown-it

- syntax highlighting
- node.js
- cmd/VScode
- tasks

    [Markdown-It download](https://github.com/markdown-it/markdown-it)

## markdown HTML extensie
**visual studio code**

- links worden niet omgezet
- syntax highlighting
- html preview

## markdown monster
**eigen platform**

- syntax highlighting
- beter HTML bestand

    [Markdown Monster download](https://markdownmonster.west-wind.com/)

## AtomIO
**eigen platform**

- syntax highlighting
- geen html highlighting

    [Atom.io download](https://atom.io/)

## markdown editor
**visual studio**

(nog niet getest)

## problemen

- links verwijzen nog naar oorspronkelijke bestanden

## links

- [Top 10 static generators](https://www.netlify.com/blog/2017/05/25/top-ten-static-site-generators-of-2017/)

- [Markdown-it](https://github.com/markdown-it)

- [Markdown Monster](https://markdownmonster.west-wind.com/)

- [Atom.io](https://atom.io/)