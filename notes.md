# Pieter Hellemans

## Aline

- terminals
- klanten
- product
- C#, Winforms, WPF

## Setup

- Core
- Operation
- Kiosk
- Portal

## Manual

- nu: weinig
- sprint > spike

## requirements

- md = input
- no setup: static html
- syntax highlighting
  - ``` var brol = 1+1; ```
- images
- internal links
- Agidens theme
- plays nice with Visual Studio Code

## Output voor ons

- onderzoek : +/-
  - https://www.staticgen.com/
    - http://www.mkdocs.org/
    - wyam
    - https://github.com/mixu/markdown-styles

- voorstel organisatie/structure
  - links > "home"

- TeamCity integration

## output voor school?

- ?
